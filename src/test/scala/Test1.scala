import org.junit.Test
import org.junit.Assert.*
import munit.CatsEffectSuite
import cats.effect.{IO, SyncIO}

class Test1 extends CatsEffectSuite:
  test("make sure IO computes the right result") {
    IO.pure(1).map(_ + 2) flatMap { result =>
      IO(assertEquals(result, 3))
    }
  }
