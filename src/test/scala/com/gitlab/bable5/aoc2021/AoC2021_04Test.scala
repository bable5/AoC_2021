package com.gitlab.bable5.aoc2021

class AoC2021_04Test extends munit.FunSuite {
  test("Part two, last winner") {
    // 3   15   0*  2*  22
    // 9*  18   13* 17* 5*
    // 19  8    7*  25  23*
    // 20  11*  10* 24* 4*
    // 14* 21*  16* 12  6

    val board = Board(
      Array(
        Array(BoardItem(3, false),  BoardItem(15, false), BoardItem(0, true),  BoardItem(2, true),   BoardItem(22, false)),
        Array(BoardItem(9, true),   BoardItem(18, false), BoardItem(13, true), BoardItem(17, true),  BoardItem(5, true)),
        Array(BoardItem(19, false), BoardItem(8, false),  BoardItem(7, true),  BoardItem(25, false), BoardItem(23, true)),
        Array(BoardItem(20, false), BoardItem(11, true),  BoardItem(10, true), BoardItem(24, true),  BoardItem(4, true)),
        Array(BoardItem(14, true),  BoardItem(21, true),  BoardItem(16, true), BoardItem(12, false), BoardItem(6, false))
      )
    )

    val winner = winningBoards(Seq(board))
    assertEquals(winner.size, 1)
    assertEquals(winner.head, board)
  }

  test("Part two, two columns") {
    val board = Board(
      Array(
        Array(BoardItem(3, false),  BoardItem(0, true)),
        Array(BoardItem(9, true),   BoardItem(13, true)),
        Array(BoardItem(19, false), BoardItem(7, true)),
        Array(BoardItem(20, false), BoardItem(10, true)),
        Array(BoardItem(14, true),  BoardItem(16, true))
      )
    )

    val winner = winningBoards(Seq(board))
    assertEquals(winner.size, 1)
    assertEquals(winner.head, board)
  }

  test("Part two, three columns") {
    val board = Board(
      Array(
        Array(BoardItem(3, false), BoardItem(0, true),  BoardItem(22, false)),
        Array(BoardItem(9, false),   BoardItem(13, true),BoardItem(5, true)),
        Array(BoardItem(19, false), BoardItem(7, true),  BoardItem(23, true)),
        Array(BoardItem(20, false),  BoardItem(10, true),BoardItem(4, true)),
        Array(BoardItem(14, true),  BoardItem(16, true), BoardItem(6, false))
      )
    )

    val winner = winningBoards(Seq(board))
    assertEquals(winner.size, 1)
    assertEquals(winner.head, board)
  }
}
