package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_13.*

class AoC2021_13Test extends munit.FunSuite {
  private val givenInput = s"""6,10
                      |0,14
                      |9,10
                      |0,3
                      |10,4
                      |4,11
                      |6,0
                      |6,12
                      |4,1
                      |0,13
                      |10,12
                      |3,4
                      |3,0
                      |8,4
                      |1,10
                      |2,14
                      |8,10
                      |9,0
                      |
                      |fold along y=7
                      |fold along x=5""".stripMargin
    .split("\n")

  test("Test example input") {
    val parsedInput: PuzzleInput = parseInput(givenInput)

    assertEquals(parsedInput.folds, Seq(Fold("y", 7), Fold("x", 5)))
    assertEquals(
      parsedInput.board.coords,
      Set(
        Coord(6, 10),
        Coord(0, 14),
        Coord(9, 10),
        Coord(0, 3),
        Coord(10, 4),
        Coord(4, 11),
        Coord(6, 0),
        Coord(6, 12),
        Coord(4, 1),
        Coord(0, 13),
        Coord(10, 12),
        Coord(3, 4),
        Coord(3, 0),
        Coord(8, 4),
        Coord(1, 10),
        Coord(2, 14),
        Coord(8, 10),
        Coord(9, 0)
      )
    )
    assertEquals(parsedInput.board.maxXY, Coord(10, 14))
  }

  test("Pretty Print, example 1") {
    val board = Board(
      Set(
        Coord(0, 0),
        Coord(4, 1)
      ),
      Coord(4, 1)
    )

    val expected =
      """#....
        |....#
        |""".stripMargin

    val actual = prettyPrintBoard(board)

    assertEquals(actual, expected)
  }

  test("Project up 1") {
    val givenCoord = Coord(6, 3)
    val foldLine = 2
    val expected = Coord(6, 1)
    val actual = projectUp(2, givenCoord)
    assertEquals(actual, expected)
  }

  test("Project up 2") {
    val givenCoord = Coord(6, 4)
    val foldLine = 2
    val expected = Coord(6, 0)
    val actual = projectUp(2, givenCoord)
    assertEquals(actual, expected)
  }

  test("Project Left") {
    assertEquals(projectLeft(5, Coord(6, 0)), Coord(4, 0))
    assertEquals(projectLeft(5, Coord(10, 0)), Coord(0, 0))
    assertEquals(projectLeft(5, Coord(9, 0)), Coord(1, 0))
  }

  test("Given example, first fold, bounds") {
    val parsedInput: PuzzleInput = parseInput(givenInput)
    val fold1 = foldHorizontal(7, parsedInput.board)

    assertEquals(fold1.maxXY, Coord(10, 6))
  }

  test("Given example, first fold prettyPrint") {
    val parsedInput: PuzzleInput = parseInput(givenInput)
    val fold1 = foldHorizontal(7, parsedInput.board)
    val expectedPrettyPrint =
      """#.##..#..#.
        |#...#......
        |......#...#
        |#...#......
        |.#.#..#.###
        |...........
        |...........
        |""".stripMargin

    assertEquals(prettyPrintBoard(fold1), expectedPrettyPrint)
  }

  test("Given example, first fold, dot count") {
    val parsedInput: PuzzleInput = parseInput(givenInput)
    val fold1 = foldHorizontal(7, parsedInput.board)
    val dotCount = countDots(fold1)

    assertEquals(dotCount, 17)
  }

  test("Given example, second fold, bounds") {
    val parsedInput: PuzzleInput = parseInput(givenInput)
    val fold2 = foldVertical(5, foldHorizontal(7, parsedInput.board))

    assertEquals(fold2.maxXY, Coord(4, 6))
  }

  test("Given example, second fold prettyPrint") {
    val parsedInput: PuzzleInput = parseInput(givenInput)
    val fold2 = foldVertical(5, foldHorizontal(7, parsedInput.board))
    val expectedPrettyPrint =
      """#####
        |#...#
        |#...#
        |#...#
        |#####
        |.....
        |.....
        |""".stripMargin

    assertEquals(prettyPrintBoard(fold2), expectedPrettyPrint)
  }
}
