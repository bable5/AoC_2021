package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_07.*

class AoC2021_07Test extends munit.FunSuite {
  private val exampleInput = Seq(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)

  test("Move example to position 2 costs 37") {
    assertEquals(alignmentCost(2, exampleInput, part1AlignmentCost), 37)
  }

  test("Other given examples") {
    assertEquals(alignmentCost(1, exampleInput, part1AlignmentCost), 41)
    assertEquals(alignmentCost(3, exampleInput, part1AlignmentCost), 39)
    assertEquals(alignmentCost(10, exampleInput, part1AlignmentCost), 71)
  }

  test("Minimum movement for example 1 is 37".only) {
    assertEquals(findMinimimCost(exampleInput, part1AlignmentCost), 37)
  }
}

class AoC2021_0702Test extends munit.FunSuite {
  //(start, end, cost)
  val givenExamples = Seq(
    (16, 5, 66),
    (1, 5, 10),
    (2, 5, 6),
    (0, 5, 15),
    //...
    (14, 5, 45)
  )

  test("Examples") {
    for ((start, end, cost) <- givenExamples)
      do assertEquals(part2AlignmentCost(start, end), cost)
  }
}
