package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_12.*
import munit.Clue.generate

class AoC2021_12Test extends munit.FunSuite {
  test("Base case. start -> cave -> end") {
    val nodes = Map(
      ("start" -> Seq("cave")),
      ("cave" -> Seq("end"))
    )

    val paths = pathsThrough(nodes)
    assertEquals(paths, Seq(Seq("start", "cave", "end")))
  }

  test("Example 1, A side.") {
    val nodes = Map(
      ("start" -> Seq("A")),
      ("A", Seq("c", "end")),
      ("c", Seq("A"))
    )

    val paths = pathsThrough(nodes)
    assertEquals(paths.size, 2)
  }

  test("Example 1, b side.") {
    val nodes = Map(
      "start" -> Seq("b"),
      "b" -> Seq("end")
    )

    val paths = pathsThrough(nodes)
    assertEquals(paths.size, 1)
  }

  test("Example 1.") {
    val nodes = Map(
      ("start" -> Seq("A", "b")),
      ("A", Seq("c", "b", "end")),
      ("b", Seq("A", "d", "end")),
      ("c", Seq("A")),
      ("d", Seq("b"))
    )

    val paths = pathsThrough(nodes)
    assertEquals(paths.size, 10)
  }

  test("Example 2") {
    val nodes =
      """dc-end
        |HN-start
        |start-kj
        |dc-start
        |dc-HN
        |LN-dc
        |HN-end
        |kj-sa
        |kj-HN
        |kj-dc""".stripMargin
        .split("\n")
        .foldLeft(Map())(parse)

    val paths = pathsThrough(nodes)
    assertEquals(paths.size, 19)
  }

  test("Example 3") {
    val nodes =
      """fs-end
        |he-DX
        |fs-he
        |start-DX
        |pj-DX
        |end-zg
        |zg-sl
        |zg-pj
        |pj-he
        |RW-he
        |fs-DX
        |pj-RW
        |zg-RW
        |start-pj
        |he-WI
        |zg-he
        |pj-fs
        |start-RW""".stripMargin
        .split("\n")
        .foldLeft(Map())(parse)

    val paths = pathsThrough(nodes)
    assertEquals(paths.size, 226)
  }

  test("Simple Parse input") {
    val givenEdge = "start-A"
    val actual = parse(Map(), givenEdge)

    val expected = Map(
      "start" -> Seq("A"),
      "A" -> Seq("start")
    )

    assertEquals(actual, expected)
  }

  test("Parse Input") {
    val inputString =
      """start-A
        |start-b
        |A-c
        |A-b
        |b-d
        |A-end
        |b-end""".stripMargin

    val actual = inputString
      .split("\n")
      .foldLeft(Map())(parse)

    val expected = Map(
      ("start" -> Seq("A", "b")),
      "A" -> Seq("start", "c", "b", "end"),
      "b" -> Seq("start", "A", "d", "end"),
      "c" -> Seq("A"),
      "d" -> Seq("b"),
      "end" -> Seq("A", "b")
    )

    println(actual)
    assertEquals(actual, expected)
  }
}
