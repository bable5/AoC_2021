package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_20.*

class AoC2021_20Test extends munit.FunSuite {
  val testInput =
    """#..#.
      |#....
      |##..#
      |..#..
      |..###""".stripMargin

  //Test input has some new lines. Remove them.
  val testInputAlg = parsePixels(
    """..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
                       |#..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
                       |.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
                       |.#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
                       |.#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
                       |...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
                       |..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#""".stripMargin
      .split("")
      .filter(s => s != "\n")
      .mkString
  )

  test("Sanity check input alg. There are 512 characters.") {
    assertEquals(testInputAlg.length, 512)
  }

  test("Parse the input image") {
    val actualImage = parseInputImage(testInput.split("\n"))
    val expectedImage: IndexedSeq[IndexedSeq[Byte]] = IndexedSeq(
      IndexedSeq(1, 0, 0, 1, 0),
      IndexedSeq(1, 0, 0, 0, 0),
      IndexedSeq(1, 1, 0, 0, 1),
      IndexedSeq(0, 0, 1, 0, 0),
      IndexedSeq(0, 0, 1, 1, 1)
    )

    assertEquals(actualImage, expectedImage)
  }

  test("Test image, middle pixel 'lookup value'") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((2, 2), testImage)

    val expected = 34

    assertEquals(idx, expected)
  }

  test("Test image, middle pixel new value") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((2, 2), testImage)
    val nextPixel = enhancePixel(idx, testInputAlg)

    val expected: Byte = 1

    assertEquals(nextPixel, expected)
  }

  test("Test image, top-left 'lookup value'") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((0, 0), testImage)

    val expected = Integer.parseInt("000010010", 2)

    assertEquals(idx, expected)
  }

  test("Test image, top-right 'lookup value'") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((0, 4), testImage)

    val expected = Integer.parseInt("000100000", 2)

    assertEquals(idx, expected)
  }

  test("Test image, bottom-left 'lookup value'") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((4, 0), testImage)

    val expected = Integer.parseInt("000000000", 2)

    assertEquals(idx, expected)
  }

  test("Test image, bottom-right 'lookup value'") {
    val testImage = parseInputImage(testInput.split("\n"))
    val idx = computeLookup((4, 4), testImage)

    val expected = Integer.parseInt("000110000", 2)

    assertEquals(idx, expected)
  }

  test("Test image, 1 iteration") {
    val testImage = parseInputImage(testInput.split("\n"))
    val nextImage = enhanceImage(testImage, testInputAlg)

    println(prettyPrint(testImage))
    println("---------------------")
    println(prettyPrint(nextImage))
  }

  test("Test image, 2 iterations, on count") {
    val testImage = parseInputImage(testInput.split("\n"))
    val nextImage =
      enhanceImage(enhanceImage(testImage, testInputAlg), testInputAlg)

    println(prettyPrint(nextImage))

    assertEquals(countOnPixel(nextImage), 35)
  }

  test("Left shifting?") {
    val two = 1 << 1
    val four = 1 << 2
    val six = two | four

    assertEquals(six, 6)
  }

  //TODO: Boundary, top right, top left, bottom right, bottom left

  //TODO: Organization? There's several core functions here

  // Given an image and a pixel, compute the 'enhancement address'

  // Given a pixel location, compute the 3 x 3 squares

}
