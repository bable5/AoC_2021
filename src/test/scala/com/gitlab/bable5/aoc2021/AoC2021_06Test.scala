package com.gitlab.bable5.aoc2021

class AoC2021_06Test extends munit.FunSuite{
    private val testInput = Seq(3,4,3,1,2)

    test("Input example, 1 day") {
        val actual = solveDay0601(1, testInput)
        val expected = Seq(2,3,2,0,1)

        assertEquals(actual, expected)
    }

    test("Input example, 2 days") {
        val actual = solveDay0601(2, testInput).sorted
        val expected = Seq(1,2,1,6,0,8).sorted

        assertEquals(actual, expected)
    }

    test("Input example, 3 days") {
        val actual = solveDay0601(3, testInput).sorted
        val expected = Seq(0,1,0,5,6,7,8).sorted

        assertEquals(actual, expected)
    }

    test("Input example, 17 days") {
        val actual = solveDay0601(17, testInput).sorted
        val expected = Seq(0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8).sorted

        assertEquals(actual, expected)
    }
}

class AoC2021_06Part2Test extends munit.FunSuite{
    private val testInput = Seq(3,4,3,1,2)

    test("Input example, 1 day") {
        val actual = solveDay0602(1, testInput)
        val expected = 5L

        assertEquals(actual, expected)
    }

    test("Input example, 2 days") {
        val actual = solveDay0602(2, testInput)
        val expected = 6L

        assertEquals(actual, expected)
    }

    test("Input example, 3 days") {
        val actual = solveDay0602(3, testInput)
        val expected = 7L

        assertEquals(actual, expected)
    }

    test("Input example, 80 days") {
        val actual = solveDay0602(80, testInput)
        val expected = 5934L

        assertEquals(actual, expected)
    }

    test("Input example, 256 days") {
        val actual = solveDay0602(256, testInput)
        val expected = 26984457539L

        assertEquals(actual, expected)
    }
}
