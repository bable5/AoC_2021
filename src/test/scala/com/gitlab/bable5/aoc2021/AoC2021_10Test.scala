package aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_10

class AoC2021_10Test extends munit.FunSuite {
  val solver = new AoC2021_10()

  test("Basic example 1") {
    val input = "{]"
    val expected = Some(("}", "]"))

    val actual = solver.checkInputPart1(input)
    println(actual)
    assertEquals(actual, expected)
  }

  test("Corrupted Example 1") {
    val input = "{([(<{}[<>[]}>{[]{[(<()>"
    val expected = Some(("]", "}"))

    val actual = solver.checkInputPart1(input)
    println(actual)
    assertEquals(actual, expected)
  }

  test("Invalid Example 1") {
    val input = "[({(<(())[]>[[{[]{<()<>>"
    val expected = None

    val actual = solver.checkInputPart1(input)
    assertEquals(actual, expected)
  }

  test("Complete Example 1") {
    val input = "[({(<(())[]>[[{[]{<()<>>"
    val expected = "}}]])})]".split("").toSeq

    val actual = solver.completeInput(input)

    assertEquals(actual, expected)
  }

  test("Complete Example 2") {
    val input = "<{([{{}}[<[[[<>{}]]]>[]]"
    val expected = "])}>".split("").toSeq

    val actual = solver.completeInput(input)

    assertEquals(actual, expected)
  }

  test("Score something simple") {
    assertEquals(
      solver.scorePart2("])}>".split("")),
      294L
    )
  }

  test("score the given examples") {
    assertEquals(
      solver.scorePart2("}}]])})]".split("")),
      288957L,
      "Example 1"
    )

    assertEquals(
      solver.scorePart2(")}>]})".split("")),
      5566L,
      "Example 2"
    )

    assertEquals(
      solver.scorePart2("}}>}>))))".split("")),
      1480781L,
      "Example 3"
    )

    assertEquals(
      solver.scorePart2("]]}}]}]}>".split("")),
      995444L,
      "Example 4"
    )

    assertEquals(
      solver.scorePart2("])}>".split("")),
      294L,
      "Example 5"
    )
  }
}
