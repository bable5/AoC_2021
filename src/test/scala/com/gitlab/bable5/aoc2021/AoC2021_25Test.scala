package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_25.*
import munit.FunSuite

class AoC2021_25Test extends FunSuite {

  test("Basic eastern movement") {
    val input = IndexedSeq(parseRow(">."))
    val expected = IndexedSeq(parseRow(".>"))

    val actual: Grid[SeaCucumber] = move(input)

    assertEquals(actual, expected)
  }

  test("Simple example 1 step") {
    val input = IndexedSeq(parseRow("...>>>>>..."))
    val expected = IndexedSeq(parseRow("...>>>>.>.."))

    val actual: Grid[SeaCucumber] = move(input)

    assertEquals(actual, expected)
  }

  test("Simple example 2 step") {
    val input = IndexedSeq(parseRow("...>>>>>..."))
    val expected = IndexedSeq(parseRow("...>>>.>.>."))

    val actual: Grid[SeaCucumber] = move(move(input))

    assertEquals(actual, expected)
  }

  test("Simple south movement") {
    val input = IndexedSeq(
      IndexedSeq(South),
      IndexedSeq(Empty)
    )
    val expected = IndexedSeq(
      IndexedSeq(Empty),
      IndexedSeq(South)
    )

    val actual: Grid[SeaCucumber] = move(input)

    assertEquals(actual, expected)
  }

  test("Simple south and east movement") {
    val input =
      """v.
        |>.""".stripMargin
        .split("\n")
        .map(parseRow)
        .toIndexedSeq
    val expected =
      """..
        |v>""".stripMargin
        .split("\n")
        .map(parseRow)
        .toIndexedSeq
  }

  test("Simple east and south movement") {
    val input = """..........
                  |.>v....v..
                  |.......>..
                  |..........""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq
    val expected = """..........
                     |.>........
                     |..v....v>.
                     |..........""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq

    val actual = move(input)

    assertEquals(actual, expected)
  }

  test("Longer example, 4 steps") {
    val input = """...>...
                  |.......
                  |......>
                  |v.....>
                  |......>
                  |.......
                  |..vvv..""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq
    val expected = """>......
                     |..v....
                     |..>.v..
                     |.>.v...
                     |...>...
                     |.......
                     |v......""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq

    val actual = (0 until 4).foldLeft(input){(acc, _) => move(acc)}
    assertEquals(actual, expected)
  }

  test("Stopping example") {
    val input = """v...>>.vv>
                  |.vv>>.vv..
                  |>>.>v>...v
                  |>>v>>.>.v.
                  |v>v.vv.v..
                  |>.>>..v...
                  |.vv..>.>v.
                  |v.v..>>v.v
                  |....v..v.>""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq
    val expected = """..>>v>vv..
                     |..v.>>vv..
                     |..>>v>>vv.
                     |..>>>>>vv.
                     |v......>vv
                     |v>v....>>v
                     |vvv.....>>
                     |>vv......>
                     |.>v.vv.v..""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq

    val actual = (0 until 58).foldLeft(input){(acc, _) => move(acc)}
    assertEquals(actual, expected)
  }

  test("Stopping example, count") {
    val input = """v...>>.vv>
                  |.vv>>.vv..
                  |>>.>v>...v
                  |>>v>>.>.v.
                  |v>v.vv.v..
                  |>.>>..v...
                  |.vv..>.>v.
                  |v.v..>>v.v
                  |....v..v.>""".stripMargin
      .split("\n")
      .map(parseRow)
      .toIndexedSeq

    val expectedMoves = 58
    val actual = moveUntilSteadyState(input)
    assertEquals(actual, expectedMoves)
  }
}
