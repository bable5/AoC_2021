package com.gitlab.bable5.aoc2021

import org.junit.Assert.{assertFalse, assertTrue}
import com.gitlab.bable5.aoc2021.AoC2021_11.printGrid

class AoC2021_11Test extends munit.FunSuite {

  import com.gitlab.bable5.aoc2021.AoC2021_11.step

  test("Example 1, 1 Step".only) {
    val input =
      """11111
        | 19991
        | 19191
        | 19991
        | 11111""".stripMargin.parseInput

    printGrid(input)
    println("\n------------\n")
    val flashCount = step(input)
    printGrid(input)
    println(flashCount)

    println("\n------------\n")
    step(input)
    printGrid(input)
  }

  test("Increment Neighbors") {
    import com.gitlab.bable5.aoc2021.AoC2021_11.incrementNeighbors
    val input = Array(
      Array(2, 2, 2, 2, 2),
      Array(2, 10, 10, 10, 2)
    )

    incrementNeighbors(1, 1, input)
    printGrid(input)
    assertEquals(
      input,
      Array(
        Array(3, 3, 3, 2, 2),
        Array(3, 10, 11, 10, 2)
      )
    )

    printGrid(input)
    incrementNeighbors(1, 2, input)
    assertEquals(
      input,
      Array(
        Array(3, 4, 4, 3, 2),
        Array(3, 11, 11, 11, 2)
      )
    )
  }

  test("Inbounds") {
    import com.gitlab.bable5.aoc2021.AoC2021_11.inBounds

    val input = Array(
      Array(2, 2, 2, 2, 2),
      Array(2, 10, 10, 10, 2)
    )

    assertTrue(inBounds(0, 0, input))
    assertTrue(inBounds(0, 1, input))
    assertTrue(inBounds(0, 2, input))
    assertTrue(inBounds(0, 3, input))

    assertFalse(inBounds(-1, 0, input))
    assertFalse(inBounds(0, -1, input))

    assertFalse(inBounds(2, 0, input))
    assertFalse(inBounds(0, 6, input))
  }

  test("Example 2, 1 Step") {
    val input: Array[Array[Int]] =
      """5483143223
        |2745854711
        |5264556173
        |6141336146
        |6357385478
        |4167524645
        |2176841721
        |6882881134
        |4846848554
        |5283751526""".stripMargin.parseInput

    val flashCount = step(input)
    printGrid(input)
    assertEquals(flashCount, 0)
  }

  test("Example 2, 2 steps".only) {
    val input: Array[Array[Int]] =
      """5483143223
        |2745854711
        |5264556173
        |6141336146
        |6357385478
        |4167524645
        |2176841721
        |6882881134
        |4846848554
        |5283751526""".stripMargin.parseInput
    println("-----------------------------------")
    printGrid(input)
    println("-----------------------------------")
    step(input)
    printGrid(input)
    val flashCount = step(input)
    println("-----------------------------------")
    printGrid(input)
    assertEquals(flashCount, 35)
  }

  extension (s: String)
    def parseInput = s
      .split("\n")
      .map(
        _.trim
          .split("")
          .map(_.toInt)
      )
      .map(_.toArray)



}
