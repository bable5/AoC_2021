package com.gitlab.bable5.aoc2021

import munit.FunSuite
import com.gitlab.bable5.aoc2021.AoC2021_16.*

class AoC2021_16Test extends FunSuite {
  test("first example to binary string") {
    assertEquals(toBinaryString("D2FE28"), "110100101111111000101000")
  }

  test("parse first example packet") {
    val input = toBinaryString("D2FE28")

    val (p, rest)  = makePacket(input)

    assertEquals(p.versionId, 6)
    assertEquals(p.typeId, 4)
    assertEquals(rest, "000")
    assertEquals(p.asInstanceOf[LiteralPacket].value, 2021L)
  }

  test("operator packet example 1") {
    val input = toBinaryString("38006F45291200")

    val (p, rest) = makePacket(input)
    assertEquals(p.versionId, 1)
    assertEquals(p.typeId, 6)
    assertEquals(rest, "0000000")

    assertEquals(p, OperatorPacket(versionId = 1, typeId = 6,
      subpackets = LiteralPacket(6, 4, 10) :: LiteralPacket(2, 4, 20) :: Nil))
  }

  test("operator packet example 2") {
    val input = toBinaryString("EE00D40C823060")

    val expected = OperatorPacket(versionId = 7, typeId = 3, subpackets =
      Seq(
        LiteralPacket(2, 4, 1),
        LiteralPacket(4, 4, 2),
        LiteralPacket(1, 4, 3),
      )
    )

    val (p, rest) = makePacket(input)
    assertEquals(rest, "00000")
    assertEquals(p, expected)
  }

  test("Version Sum Literal") {
    assertEquals(LiteralPacket(100, 2, 1234).versionSum, 100L)
  }

  test("Version Sum Operator") {
    val p = OperatorPacket(
      1, 0, Seq(
        OperatorPacket(3, 0, Seq(LiteralPacket(5, 0, 0))),
        LiteralPacket(7, 0 , 0)
      )
    )

    assertEquals(p.versionSum, 16L)
  }

  test("Example 8A004A801A8002F478 version sum is 16") {
    val input = toBinaryString("8A004A801A8002F478")

    val (p, _) = makePacket(input)
    println(p)
    assertEquals(p.versionSum, 16L)
  }

  test("Example 620080001611562C8802118E34 version sum is 12") {
    val input = toBinaryString("620080001611562C8802118E34")

    val (p, _) = makePacket(input)
    println(p)
    assertEquals(p.versionSum, 12L)
  }

  test("Example C0015000016115A2E0802F182340 version sum is 23") {
    val input = toBinaryString("C0015000016115A2E0802F182340")

    val (p, _) = makePacket(input)
    println(p)
    assertEquals(p.versionSum, 23L)
  }

  test("Example A0016C880162017C3686B18A3D4780 version sum is 31") {
    val input = toBinaryString("A0016C880162017C3686B18A3D4780")

    val (p, _) = makePacket(input)
    println(p)
    assertEquals(p.versionSum, 31L)
  }

  test("Bounds?"){
    1 to 2 foreach(println)
  }

  test("take and drop behavior") {
    val input = "abcdefgh"
    val first = input.take(3)
    val rest = input.drop(3)

    assertEquals(first, "abc")
    assertEquals(rest, "defgh")
  }

  test("Main run error") {
    val input = "11011111100000011010100011100010"
    println(s"${input.length}")
    java.lang.Long.parseLong(input, 2)
  }

  test("Interpret sum") {
    runInterpretationTest("C200B40A82", 3)
  }

  test("Interpret product") {
    runInterpretationTest("04005AC33890", 54)
  }

  test("Interpret min") {
    runInterpretationTest("880086C3E88112", 7)
  }

  test("Interpret max") {
    runInterpretationTest("CE00C43D881120", 9)
  }

  test("Interpret literal".ignore) {
    runInterpretationTest("C200B40A82", 3)
  }

  test("Interpret greater than") {
    runInterpretationTest("D8005AC2A8F0", 1)
  }

  test("Interpret less lan") {
    runInterpretationTest("F600BC2D8F", 0)
  }

  test("Interpret equal") {
    runInterpretationTest("9C005AC2F8F0", 0)
  }

  private def runInterpretationTest(s: String, expected: Long) =
    val input = toBinaryString(s)

    val (p, _) = makePacket(input)
    println(p)

    val actual = p.value

    assertEquals(actual, expected)

}
