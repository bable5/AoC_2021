package aoc2021

import com.gitlab.bable5.aoc2021.ResourceFileReader

class ResourceFileReaderTest extends munit.FunSuite {
  test("Read the lines in a resource") {
      val input = ResourceFileReader("sample-input")
      assertEquals(input.toSeq, Seq("1", "2", "abc"))
  }
}
