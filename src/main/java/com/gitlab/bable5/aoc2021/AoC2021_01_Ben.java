package com.gitlab.bable5.aoc2021;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * Port of Ben McHone's solution.
 */
public class AoC2021_01_Ben {
    private final DeteriministicDie die = new DeteriministicDie();

    public String playDeterministic(int p1_loc, int p2_loc, String prefix) {
        var player = 0;
        var rolls = 0;
        var locations = new int[]{p1_loc, p2_loc};
        var scores = new int[]{0, 0};

        while (maxScore(scores) < 1000) {
            for(int i = 0; i < 3; i++) {
                locations[player] = locations[player] + die.rollDeterministic();
                while(locations[player] > 10) { //TODO: Funny way to mod.
                    locations[player] = locations[player] - 10;
                }
            }

            rolls += 3;
            scores[player] += locations[player];
            player = (player + 1) % 2;
        }
        return String.format("%s%d", prefix, minScore(scores) * rolls);
    }

    private int maxScore(int[] scores) {
        return Arrays.stream(scores)
                .max()
                .orElseThrow(NoSuchElementException::new);
    }

    private int minScore(int[] scores) {
        return Arrays.stream(scores)
                .min()
                .orElseThrow(NoSuchElementException::new);
    }

    private static class DeteriministicDie {
        private int value = 0;

        public int rollDeterministic() {
            if(value == 100) {
                value = 0;
            }
            value++;

            return value;
        }
    }
}
