package com.gitlab.bable5.aoc2021

extension [T: Numeric](values: Iterator[T])
  def solveDay1 = {
    import math.Ordering.Implicits.infixOrderingOps
    val (_, count) = values.foldLeft((None: Option[T], 0)) { (acc, next) =>
      var (prevMin, count) = acc
      prevMin match {
        case Some(prev) if next > prev =>
          (Option(next), count + 1)
        case None =>
          (Option(next), 0)
        case _ =>
          (Option(next), count)
      }
    }
    count
  }

@main def day0101: Unit =
  val count = ResourceFileReader("day01.input")
    .map(_.toLong)
    .solveDay1

  println(count)

@main def day0102: Unit =
  val count = ResourceFileReader("day01.input")
    .map(_.toLong)
    .sliding(3)
    .map(_.sum)
    .solveDay1
  println(count)
