package com.gitlab.bable5.aoc2021

import scala.annotation.tailrec

case class BoardItem(value: Int, checked: Boolean = false)

case class Board(squares: Array[Array[BoardItem]])

def scoreWinningBoardPart1(winningNumber: Int, b: Board): Int = {
  val unmarkedSquares = b.squares.flatten.toSeq.filter(!_.checked).map(_.value)
  println(s"There are ${unmarkedSquares.length} unmarked")
  val sumUnmarked = unmarkedSquares.sum
  println(s"Sum unmarked: $sumUnmarked")
  sumUnmarked * winningNumber
}

def render(b: Board): String =
  b.squares.foldLeft("") { (acc, next) =>
    acc + "\n" + next.foldLeft("") { (acc2, nextItem) =>
      acc2 + " " + (if nextItem.checked then nextItem.value + "*" else nextItem.value)
    }
  }

@tailrec
def parseBoards(input: Seq[String], acc: Seq[Board]): Seq[Board] =
  val (nextBoard, rest) = parseBoard(input)
  val accPrime = acc.+:(nextBoard)
  if rest.isEmpty then accPrime
  else parseBoards(rest, accPrime)

def parseBoard(input: Seq[String]): (Board, Seq[String]) =
  val boardRows = input.takeWhile(!_.isEmpty).toArray
  assert(boardRows.size == 5)

  val board = Board(
    boardRows
      .map(r => r.trim)
      .map(r =>
        r.split("\\s+")
          .map(i => BoardItem(i.toInt))
      )
  )

  (board, input.drop(boardRows.size + 1))

def updateBoard(v: Int, b: Board): Board =
  Board(
    b.squares.map(r => r.map(i => if i.value == v then BoardItem(v, true) else i))
  )

def winningBoards(boards: Seq[Board]): Seq[Board] =
  def hasFullRow(board: Board): Boolean = board.squares.exists(r => r.forall(_.checked))

  def checkColumn(col: Int, board: Board): Boolean =
    var allChecked = true
    for(i <- board.squares.indices)
      do
      allChecked = allChecked && board.squares(i)(col).checked
    allChecked

  def hasFullColumn(board: Board): Boolean =
    var anyFullColumn = false
    for(col <- board.squares(0).indices)
      do
        anyFullColumn = anyFullColumn || checkColumn(col, board)
    anyFullColumn

  boards.filter(board => hasFullRow(board) || hasFullColumn(board))

@tailrec
def playBingo(picks: Seq[Int], boards: Seq[Board]): (Int, Board) =
  val nextNumber = picks.head
  val nextBoards = boards.map(b => updateBoard(nextNumber, b))
  val maybeWinner = winningBoards(nextBoards).headOption

  if maybeWinner.isDefined then (nextNumber, maybeWinner.get) else playBingo(picks.tail, nextBoards)

@tailrec
def playBingoToLastBoard(picks: Seq[Int], boards: Seq[Board]): (Int, Board) =
  println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
  val nextNumber = picks.head
  println(s"Drew $nextNumber")
  val nextBoards = boards.map(b => updateBoard(nextNumber, b))

  nextBoards.foreach(b => println(render(b)))

  val winners = winningBoards(nextBoards)

  if winners.nonEmpty  then
    println(s"removing ${winners.size} boards")
    val remainingBoards = nextBoards.filter(b => !winners.contains(b)) //TODO: Does this work?
    println(s"There are now ${remainingBoards.size} in the set")

    if remainingBoards.isEmpty && winners.size == 1 then
      println("Done!")
      println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      (nextNumber, winners.head)
    else
      println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      playBingoToLastBoard(picks.tail, remainingBoards)

  else
    println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    playBingoToLastBoard(picks.tail, nextBoards)


@main def day0401(): Unit =
  val input = ResourceFileReader("day04.input").toSeq
  val (picks, boards) = input match {
    case picks :: blank :: rest =>
      (picks.split(",").map(_.toInt), parseBoards(rest, Nil))
  }

  println(picks.toSeq)
  boards.foreach(b => println(render(b)))

  val (winningNumber, winningBoard) = playBingo(picks, boards)

  println("-----------------------------\nwinner\n---------------------------")
  println(render(winningBoard))

  println(scoreWinningBoardPart1(winningNumber, winningBoard))


@main def day0402(): Unit =
  val input = ResourceFileReader("day04.input").toSeq
  val (picks, boards) = input match {
    case picks :: blank :: rest =>
      (picks.split(",").map(_.toInt), parseBoards(rest, Nil))
  }

  val (winningNumber, winningBoard) = playBingoToLastBoard(picks, boards)

  println("-----------------------------\nwinner\n---------------------------")
  println(s"Winning number $winningNumber")
  println(render(winningBoard))

  println(scoreWinningBoardPart1(winningNumber, winningBoard))
