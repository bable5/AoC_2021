package com.gitlab.bable5.aoc2021

import com.gitlab.bable5.aoc2021.AoC2021_20.{parseInputImage, parsePixels}

import scala.collection.mutable.ArrayBuffer

object AoC2021_20 {
  private val lightPixel = "#"
  type Image = IndexedSeq[IndexedSeq[Byte]]
  type Coord = (Int, Int)

  private def lookupPixel(coord: Coord, image: Image): Int = {
    if coord._1 < 0 || coord._1 > image.size - 1 then 0
    else if coord._2 < 0 || coord._2 > image(coord._1).size - 1 then 0
    else image(coord._1)(coord._2)
  }

  def enhancePixel(idx: Int, alg: IndexedSeq[Byte]): Byte
    = alg(idx)

  def computeLookup(coord: Coord, image: Image): Int = {
    val v1 = lookupPixel((coord._1 - 1, coord._2 - 1), image) << 8
    val v2 = lookupPixel((coord._1 - 1, coord._2), image) << 7
    val v3 = lookupPixel((coord._1 - 1, coord._2 + 1), image) << 6
    // Middle
    val v4 = lookupPixel((coord._1, coord._2 - 1), image) << 5
    val v5 = lookupPixel((coord._1, coord._2), image) << 4
    val v6 = lookupPixel((coord._1, coord._2 + 1), image) << 3
    // Bottom
    val v7 = lookupPixel((coord._1 + 1, coord._2 - 1), image) << 2
    val v8 = lookupPixel((coord._1 + 1, coord._2), image) << 1
    val v9 = lookupPixel((coord._1 + 1, coord._2 + 1), image)

    v1 | v2 | v3 | v4 | v5 | v6 | v7 | v8 | v9
  }

  def enhanceImage(img: Image, alg: IndexedSeq[Byte]): Image =
    val nextImage: ArrayBuffer[ArrayBuffer[Byte]] = ArrayBuffer()
    // assume a square image
    // go 1 past the current edge of the image to account for the 'infinate' part
    val maxHeight = img(0).size
    for(i <- -1 to img.size)
      do
        val nextRow = ArrayBuffer[Byte]()
        nextImage += nextRow
        for(j <- -1 to maxHeight)
          do
          val idx = computeLookup((i, j), img)
          val nextPixel: Byte = alg(idx)
          nextRow += nextPixel
    nextImage.map(_.toIndexedSeq).toIndexedSeq

  def countOnPixel(img: Image): Int =
    img.map(_.count(_ == 1)).sum

  def prettyPrint(img: Image): String =
    img.map(_.map(b => if b == 1 then lightPixel else ".").mkString).mkString("\n")

  private def trace[A](a: A): A =
    println(a)
    a

  def parseInputImage(input: Iterable[String]): Image =
    input.map(parsePixels).toIndexedSeq

  def parsePixels(s: String): IndexedSeq[Byte] = s
      .split("")
      .map { s =>
        if s == lightPixel then 1.toByte
        else 0.toByte
      }
      .toIndexedSeq
}

@main def aoc2021_2001(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_20.*

  val input = ResourceFileReader("day20.input")
  val enhanceAlg = parsePixels(input.next())

  input.drop(1)

  val inputImage = parseInputImage(input.to(Iterable))

  val enhanced = enhanceImage(enhanceImage(inputImage, enhanceAlg), enhanceAlg)

  println(prettyPrint(inputImage))
  println("--------------------------------------------")
  println(prettyPrint(enhanced))

  println(countOnPixel(enhanced))
