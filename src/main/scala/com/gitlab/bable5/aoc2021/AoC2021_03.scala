package com.gitlab.bable5.aoc2021

import scala.collection.mutable.ArrayBuffer
import scala.annotation.tailrec

// TODO Generalizes to a monoid. Do some type summoning later
def countPosition(pos: Int, items: Seq[IndexedSeq[Int]]): Map[Int, Int] =
    items.foldLeft(Map[Int, Int]()){ (acc, next) =>
        val elem = next(pos)
        val last = acc.get(elem)
        if(last.isDefined) 
            acc ++ Map(elem -> (last.get + 1))
        else 
            acc ++ Map(elem -> 1)
    }

def computeGammaRate(counts: IndexedSeq[Map[Int, Int]]): Int =
    val binString = counts.foldLeft(""){(acc, next) => 
        //find the key with the largest value
        val (k, v) = next.foldLeft((0,0)){(acc2, next2) => if next2._2 > acc2._2 then next2 else acc2 }
        s"$acc$k"
    }
    Integer.parseInt(binString, 2)

def computeEpsilonRate(counts: IndexedSeq[Map[Int, Int]]): Int =
    val binString = counts.foldLeft(""){(acc, next) => 
        //find the key with the smalles value
        val (k, v) = next.foldLeft((0,Int.MaxValue)){(acc2, next2) => if next2._2 < acc2._2 then next2 else acc2 }
        s"$acc$k"
    }
    Integer.parseInt(binString, 2)

def countDigits(input: Seq[IndexedSeq[Int]]): IndexedSeq[Map[Int, Int]] =
    val lineLength = input.head.size
    val digitCounts = new ArrayBuffer[Map[Int, Int]](lineLength)
    for(i <- 0 to (lineLength - 1 )) do 
        val counts = countPosition(i, input)
        digitCounts.addOne(counts)
    digitCounts.toIndexedSeq

@main def day0301(): Unit =
    val input = ResourceFileReader("day03.input")
        .map(s => s.split("").map(_.toInt).toIndexedSeq)
        .toSeq
    
    val digitCounts = countDigits(input)
    
    val gammaRate = computeGammaRate(digitCounts)
    val epsilonRate = computeEpsilonRate(digitCounts)

    println(s"GammaRate: $gammaRate")
    println(s"EpsilonRate: $epsilonRate")
    println(s"Solution: ${gammaRate * epsilonRate}")

def oxygenRating(input: Seq[IndexedSeq[Int]]): Int = 
    def preferHigherCount(m: Map[Int, Int]): Int = if m(0) > m(1) then 0 else 1
    part2Helper(0, input, preferHigherCount)

def co2ScrubberRating(input: Seq[IndexedSeq[Int]]): Int = 
    def preferLowestCount(m: Map[Int, Int]): Int = if m(0) <= m(1) then 0 else 1
    part2Helper(0, input, preferLowestCount)

@tailrec
private def part2Helper(idx: Int, input: Seq[IndexedSeq[Int]], positionMatcher: Map[Int, Int] => Int): Int =
    val usageInfo = countDigits(input)
    val freqs = usageInfo(idx)
    val positionMustMatch = positionMatcher(freqs)
    val next = input.filter(v => v(idx) == positionMustMatch)
    if next.size == 1 then
        val res = next.head.toSeq.mkString
        Integer.parseInt(res, 2)
    else
        part2Helper(idx + 1, next, positionMatcher)

@main def day0302(): Unit = 
    val input = ResourceFileReader("day03.input")
        .map(s => s.split("").map(_.toInt).toIndexedSeq)
        .toSeq
    
    val oxygen = oxygenRating(input)
    val co2 = co2ScrubberRating(input)
    println(oxygen * co2)