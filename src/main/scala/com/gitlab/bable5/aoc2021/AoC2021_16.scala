package com.gitlab.bable5.aoc2021

import java.lang
import scala.annotation.tailrec
import scala.collection.mutable

object AoC2021_16 {
  trait Packet {
    //TODO: Scala 3 feature with trait params?
    val versionId: Int
    val typeId: Int
    lazy val versionSum: Long
    def value: Long
  }

  case class LiteralPacket(versionId: Int, typeId: Int, value: Long) extends Packet {
    override lazy val versionSum: Long = versionId
  }

  case class OperatorPacket(versionId: Int, typeId: Int, subpackets: Seq[Packet]) extends Packet {
    override lazy val versionSum: Long = versionId + subpackets.foldLeft(0L) { (acc, next) =>
      acc + next.versionSum
    }

    override lazy val value: Long = typeId match {
      case 0 => subpackets.map(_.value).sum
      case 1 => subpackets.map(_.value).product
      case 2 => subpackets.map(_.value).min
      case 3 =>  subpackets.map(_.value).max
      case 4 => throw new IllegalArgumentException()
      case 5 => val v1 = subpackets.head.value
        val v2 = subpackets.tail.head.value
        if v1 > v2 then 1 else 0
      case 6 => val v1 = subpackets.head.value
        val v2 = subpackets.tail.head.value
        if v1 < v2 then 1 else 0
      case 7 => val v1 = subpackets.head.value
        val v2 = subpackets.tail.head.value
        if v1 == v2 then 1 else 0
    }
  }

  extension (s: String)
    def leftPad(pad: Char, n: Int): String = if s.length >= n then s
    else {
      val needed = n - s.length
      val builder = mutable.StringBuilder()
      for (_ <- 0 until needed) {
        builder += pad
      }
      builder ++= s
      builder.toString
    }

  def toBinaryString(s: String): String =
    s.split("")
      .map(
        Integer
          .parseInt(_, 16)
          .toBinaryString
          .leftPad('0', 4)
      )
      .mkString("")

  def literalPacket(versionId: Int, typeId: Int, encoded: String): (Packet, String) = {
    def parseLiteral(s: String): (String, String) = {
      val (p, rest) = s.splitAt(5)
      val (id, num) = p.splitAt(1)
      if id == "1" then
        val (next, rest2) = parseLiteral(rest)
        (num + next, rest2)
      else (num, rest)
    }

    val (l, rest) = parseLiteral(encoded)
    val literalValue = java.lang.Long.parseLong(l, 2)
    (LiteralPacket(versionId, typeId, literalValue), rest)
  }

  def operatorPacket(versionId: Int, typeId: Int, encoded: String): (Packet, String) =
    if encoded.isEmpty then throw new IllegalArgumentException()

    def parsePacketLength(bitCount: Int, encoded: String): (Int, String) =
      val (subPacketBits, rest) = encoded.splitAt(bitCount)
      val subPacketBitCount = Integer.parseInt(subPacketBits, 2)
      (subPacketBitCount, rest)

    def parseLengthType0(operatorEncoded: String): (Seq[Packet], String) =
      val (subPacketBitCount, rest) = parsePacketLength(15, operatorEncoded)
      val subPacketsEncoded = rest.take(subPacketBitCount)
      val remainingBitsToProcess = rest.drop(subPacketBitCount)
      val (subPackets, _) = makePackets(subPacketsEncoded)
      (subPackets, remainingBitsToProcess)

    def parseLengthType1(operatorEncoded: String): (Seq[Packet], String) =
      val (subPacketCount, rest) = parsePacketLength(11, operatorEncoded)
      var (acc, rest1) = (Nil: Seq[Packet], rest)

      (1 to subPacketCount).foreach { _ =>
        val (p, r) = makePacket(rest1)
        acc = acc.appended(p)
        rest1 = r
      }

      (acc, rest1)

    val (lengthTypeId, rest) = encoded.splitAt(1)
    val (subPackets, operatorRest) = lengthTypeId match {
      case "0" => parseLengthType0(rest)
      case "1" => parseLengthType1(rest)
      case _ => throw new RuntimeException(s"Unrecoqnized '$lengthTypeId'")
    }

    (OperatorPacket(versionId, typeId, subPackets), operatorRest)

  def makePackets(s: String): (Seq[Packet], String) =
    val (packet, rest) = makePacket(s)
    if rest.isEmpty then (Seq(packet), "")
    else {
      val (packet2, rest2) = makePackets(rest)
      (packet +: packet2, rest2)
    }

  def makePacket(s: String): (Packet, String) = {
    if s.isEmpty then throw new IllegalArgumentException()

    val (header, rest) = s.splitAt(6)
    val (version, pType) = header.splitAt(3)

    val v = Integer.parseInt(version, 2)
    val t = Integer.parseInt(pType, 2)

    t match {
      case 4 => literalPacket(v, t, rest)
      case _ => operatorPacket(v, t, rest)
    }
  }
}

@main def aoc2021_16(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_16.*
  val input = ResourceFileReader("day16.input").next
    .split("")
    .map(c => Integer.parseInt(c, 16))
    .map(i => i.toBinaryString.leftPad('0', 4))
    .mkString("")

  val packet = makePacket(input)
  println(s"Part 1: ${packet._1.versionSum}")
  println(s"Part 2: ${packet._1.value}")

