package com.gitlab.bable5.aoc2021

import scala.annotation.tailrec

@tailrec
def solveDay0601(counter: Int, input: Seq[Int]): Seq[Int] =
  println(s"Simulating day ${counter}. There are ${input.size} inputs")
  if counter == 0 then input
  else
    solveDay0601(
      counter - 1,
      input.foldLeft(Nil: Seq[Int]) { (acc, next) =>
        if next == 0 then Seq(6, 8) ++ acc
        else acc.:+(next - 1)
      }
    )

def day0602Helper(current: Map[Int, Long]): Map[Int, Long] = 
  val spawnCount: Long = current.getOrElse(0, 0)
  val next = (current - (0)).foldLeft(Map[Int, Long]()){(acc, next) => 
    val nextCount: Long = (current(next._1))
    acc + ((next._1 - 1)-> nextCount)
  }
  
  val sixCount: Long = next.getOrElse(6, 0L) + spawnCount

  (next + (6 -> sixCount) + (8 -> spawnCount)).filter(_._2 > 0)
  

def solveDay0602(counter: Int, input: Seq[Int]): Long =
  var counts = input.foldLeft(Map[Int, Long]()) { (acc, next) =>
    val currentCount: Long = acc.getOrElse(next, 0L)
    acc + (next -> (currentCount + 1))
  }

  for(_ <- 0 until counter)
  do 
    counts = day0602Helper(counts)
  
  counts.map(_._2).reduce(_ + _)

@main def day0601(): Unit =
  val input = ResourceFileReader("day06.input").next
    .split(",")
    .map(_.toInt)
    .toSeq

  println(solveDay0601(80, input).size)
  println("done!")


@main def day0602(): Unit =
  import com.gitlab.bable5.aoc2021.Utils.time
  val input = ResourceFileReader("day06.input").next
    .split(",")
    .map(_.toInt)
    .toSeq
  time{
    println(solveDay0602(256, input))
  }
