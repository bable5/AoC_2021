package com.gitlab.bable5.aoc2021

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer

object AoC2021_25 {
  sealed trait SeaCucumber
  case object East extends SeaCucumber
  case object South extends SeaCucumber
  case object Empty extends SeaCucumber

  def prettyPrint(g: Grid[SeaCucumber]): String = {
    g.map(r =>
      r.map {
        case East => ">"
        case South => "v"
        case Empty => "."
      }.mkString
    ).mkString("\n")
  }

  def nextIdx(x: Int, maxX: Int): Int =
    val nextX = x + 1
    if nextX < maxX then nextX else 0

  def prevIdx(x: Int, maxX: Int): Int =
    val nextX = x - 1
    if nextX < 0 then maxX else nextX

  private def moveEast(g: Grid[SeaCucumber]): Grid[SeaCucumber] =
    g.applyAll { (i, j) =>
      g(i)(j) match {
        case East => if g(i)(nextIdx(j, g(i).size)) == Empty then Empty else East
        case Empty => if g(i)(prevIdx(j, g(i).size - 1)) == East then East else Empty
        case c @ _ => c
      }
    }

  private def moveSouth(g: Grid[SeaCucumber]): Grid[SeaCucumber] =
    g.applyAll { (i, j) =>
      g(i)(j) match {
        case South => if g(nextIdx(i, g.size))(j) == Empty then Empty else South
        case Empty => if g(prevIdx(i, g.size - 1))(j) == South then South else Empty
        case c @ _ => c
      }
    }
  def move(g: Grid[SeaCucumber]): Grid[SeaCucumber] =
    (moveEast andThen moveSouth)(g)

  @tailrec
  private def moveUntilSteadStateHelper(g: Grid[SeaCucumber], count: Int): (Grid[SeaCucumber], Int) =
    val next = move(g)
    val nextCount = count + 1
    if next == g then (g, nextCount)
    else moveUntilSteadStateHelper(next, nextCount)

  def moveUntilSteadyState(g: Grid[SeaCucumber]): Int =
    moveUntilSteadStateHelper(g, 0)._2


  def parseRow(s: String): IndexedSeq[SeaCucumber] = {
    s.split("").map { s =>
      s match
        case ">" => East
        case "v" => South
        case "." => Empty
    }
  }
}

@main def aoc2021_25(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_25.*
  val input = ResourceFileReader("day25.input")
    .map(parseRow)
    .toIndexedSeq

  println(moveUntilSteadyState(input))
