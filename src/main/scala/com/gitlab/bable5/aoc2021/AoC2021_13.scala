package com.gitlab.bable5.aoc2021

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object AoC2021_13 {
  case class Coord(x: Int, y: Int)
  case class Board(coords: Set[Coord] = Set(), maxXY: Coord = Coord(0, 0))
  extension (b: Board)
    def maxX = b.coords.map(_.x).max
    def maxY = b.coords.map(_.y).max

  case class Fold(dir: String, coord: Int)
  case class PuzzleInput(board: Board = Board(), folds: Seq[Fold] = Nil)

  private val coordRegex = raw"(\d+),(\d+)".r
  private val foldRegex = raw"fold along (\w)=(\d+)".r

  def parseLine(s: String): Coord | Fold | Unit = s match { //TODO: That's a silly type
    case coordRegex(x, y) => Coord(x.toInt, y.toInt)
    case foldRegex(dir, coord) => Fold(dir, coord.toInt)
    case _ => ()
  }

  def parseInput(it: Iterable[String]): PuzzleInput =
    val puzzleInput = it.foldLeft(PuzzleInput()) { (acc, next) =>
      parseLine(next) match {
        case c: Coord => acc.copy(board = acc.board.copy(acc.board.coords + c))
        case f: Fold => acc.copy(folds = acc.folds :+ f)
        case _: Unit => acc
      }
    }

    val maxX = puzzleInput.board.maxX
    val maxY = puzzleInput.board.maxY

    puzzleInput.copy(board = puzzleInput.board.copy(maxXY = Coord(maxX, maxY)))
  //TODO Alternate representation, each row is a string and then combine the rows?

  def prettyPrintBoard(b: Board): String =
      val maxX = b.maxXY.x
      val maxY = b.maxXY.y
      val sb = mutable.StringBuilder()
      for(y <- 0 to maxY)
        for(x <- 0 to maxX)
        do
          val coordExists = b.coords.apply(Coord(x, y))
          if coordExists then sb ++= "■" else sb += ' '
        sb += '\n'
      sb.toString

  def projectUp(foldPoint: Int, c: Coord): Coord =
    c.copy(
      y = 2 * foldPoint - c.y
    )

  def projectLeft(foldPoint: Int, c: Coord): Coord =
    c.copy(x = 2 * foldPoint - c.x)

  def foldHorizontal(foldPoint: Int, b: Board): Board =
    //The true set is below the fold.
    //The false set is what we will reduce into.
    val groupedPoints = b.coords.groupBy(c => c.y > foldPoint)
    val keep = groupedPoints.getOrElse(false, Set())
    val overlay = groupedPoints.getOrElse(true, Set())

    val nextPoints = overlay.foldLeft(keep){(acc, next) =>
      acc + projectUp(foldPoint, next)
    }

    Board(nextPoints, b.maxXY.copy(y = foldPoint - 1))

  def foldVertical(foldPoint: Int, b: Board): Board =
    // assert all in row Y are empty

    //The true set is below the fold.
    //The false set is what we will reduce into.
    val groupedPoints = b.coords.groupBy(c => c.x > foldPoint)
    val keep = groupedPoints.getOrElse(false, Set())
    val overlay = groupedPoints.getOrElse(true, Set())

    val nextPoints = overlay.foldLeft(keep){(acc, next) =>
      acc + projectLeft(foldPoint, next)
    }

    Board(nextPoints, b.maxXY.copy(x = foldPoint - 1))

  def countDots(board: Board): Int =
    board.coords.size

  def foldBoard(board: Board, fold: Fold): Board = fold match {
    case Fold("x", c) => foldVertical(c, board)
    case Fold("y", c) => foldHorizontal(c, board)
  }
}

@main def day1301(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_13.*
  val PuzzleInput(board, folds) = parseInput(ResourceFileReader("day13.input").to(Iterable))

  val nextBoard = foldBoard(board, folds.head)
  println(countDots(nextBoard))

@main def day1302(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_13.*
  val PuzzleInput(board, folds) = parseInput(ResourceFileReader("day13.input").to(Iterable))

  val completedFolds = folds.foldLeft(board)(foldBoard)
  println(prettyPrintBoard(completedFolds))