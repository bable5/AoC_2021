package com.gitlab.bable5.aoc2021

object AoC2021_11 {
  def increment(x: Int, y: Int, grid: Array[Array[Int]]) = grid(x)(y) = grid(x)(y) + 1

  def inBounds(i: Int, j: Int, grid: Array[Array[Int]]): Boolean =
    i >= 0 &&
      i < grid.length &&
      j >= 0 &&
      j < grid(i).length

  def incrementNeighbors(i: Int, j: Int, grid: Array[Array[Int]]): Unit =
    for (iInc <- -1 to 1)
      for (jInc <- -1 to 1)
        val (x, y) = (i + iInc, j + jInc)
//        println(s"$x, $y")
        if (iInc != 0 || jInc != 0) && inBounds(x, y, grid) then
//          println(s"Increment $x, $y")
          increment(x, y, grid)

  //TODO: This is mutable. Immutuable solution?
  def step(grid: Array[Array[Int]]): Int = {
    var count = 0
    var prevCount = 0
    var flashedPoints = Set[(Int, Int)]()

    for (i <- grid.indices)
      for (j <- grid.indices)
        increment(i, j, grid)

    while {
      prevCount = count

      //figure out who exceded
      for (i <- grid.indices)
        for (j <- grid.indices)
          if grid(i)(j) > 9 && !flashedPoints.contains((i, j)) then
            // println(s"$i,$j flashes")
            flashedPoints = flashedPoints + ((i, j))
            incrementNeighbors(i, j, grid)
            count = count + 1

      prevCount != count
    } do ()

    flashedPoints.foreach((i, j) => grid(i)(j) = 0)

    count
  }

  def allFlashed(input: Array[Array[Int]]): Boolean = 
    input.filter(r => r.filter(v => v > 0).isEmpty).isEmpty

  def printGrid(input: Array[Array[Int]]): Unit =
    for (i <- input.indices)
      do
        for (j <- input(i).indices)
          do print(s"${input(i)(j)} ")
        println("")
}

@main def aoc2021Day11_01(): Unit = 
  import com.gitlab.bable5.aoc2021.AoC2021_11.*

  val input = ResourceFileReader("day11.input")
    .map(_.split("").map(_.toInt).toArray)
    .toArray
  var count = 0
  
  for(i <- 1 to 400)
    do
      val flashCount = step(input)
      val allFlash = flashCount == 100
      // println(s"After step; $i: FlashCount $flashCount, All Flash: ${}")
      // printGrid(input)
      // println("")
      if allFlash then println(s"All Flashed: $i")
      count = count + flashCount

  println(count)
  
  
