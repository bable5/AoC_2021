package com.gitlab.bable5.aoc2021

import scala.io.Source

object ResourceFileReader:
  def apply(file: String): Iterator[String] = Source.fromResource(file).getLines
