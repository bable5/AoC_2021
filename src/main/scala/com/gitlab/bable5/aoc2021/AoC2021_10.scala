package com.gitlab.bable5.aoc2021

import scala.collection.mutable.Stack
import scala.runtime.Statics

class AoC2021_10() {
  val closedBy = Map(
    "(" -> ")",
    "[" -> "]",
    "{" -> "}",
    "<" -> ">"
  )

  def open(elem: String, stack: Stack[String]): Stack[String] =
    stack.prepend(elem)

  def close(openElement: String, stack: Stack[String]) =
    if stack.head == openElement then (None, stack.tail)
    else (Option((stack.head, openElement)), stack)

  def checkInputPart1(s: String): Option[(String, String)] = {
    var lookingFor = new Stack[String]
    for (c <- s.split(""))
      do
        if c == "(" || c == "[" || c == "{" || c == "<" then
          lookingFor = open(closedBy(c), lookingFor)
        else
          val (err, stack) = close(c, lookingFor)
          if err.isDefined then return err
          else lookingFor = stack

    None
  }

  def completeInput(s: String): Seq[String] =
    s.split("")
      .foldLeft(new Stack[String]) { (acc, c) =>
        if c == "(" || c == "[" || c == "{" || c == "<" then open(closedBy(c), acc)
        else
          val (err, stack) = close(c, acc)
          if err.isDefined then throw new RuntimeException("Invalid string encountered")
          else stack
      }
      .toSeq

  def scorePart2(ss: Seq[String]): Long =
    val multiplier = 5L
    ss.foldLeft(0L) { (acc, next) =>

      val inc = next match {
        case ")" => 1L
        case "]" => 2L
        case "}" => 3L
        case ">" => 4L
        case ex @ _ => throw new RuntimeException(s"WTF, '${ex}'")
      }

      (acc * multiplier) + inc
    }
}

@main def aoc2021Day10_02(): Unit =
  val solver = new AoC2021_10()
  val scores = ResourceFileReader("day10.input")
    .filter(input => solver.checkInputPart1(input).isEmpty)
    .map(solver.completeInput)
    .map(solver.scorePart2)
    .toIndexedSeq
    .sorted

  println(s"Total Entries: ${scores.size}")
  for (i <- 0 to (scores.size - 1)) {
    println(s"${i + 1}: ${scores(i)}")
  }
  println(scores((scores.size) / 2))

@main def aoc2021Day10_01(): Unit =
  val solver = new AoC2021_10()
  val res = ResourceFileReader("day10.input")
    .map(solver.checkInputPart1)
    .flatten
    .map(_ match {
      case (_, ")") => 3
      case (_, "]") => 57
      case (_, "}") => 1197
      case (_, ">") => 25137
    })
    .toSeq
    .sum
  println(res)
