package com.gitlab.bable5.aoc2021

object AoC2021_12 {
  //find a path.
  // Given current node
  // Find all desitions
  // If destitation is target, done
  // Else recurse through sub paths.
  private def find(start: String, target: String, current: Seq[String], graph: Map[String, Seq[String]]): Seq[Seq[String]] =
    val inSmallCave = start.charAt(0).isLower
    val nodes = graph(start)
    val nextGraph =
      if inSmallCave then
        graph.foldLeft(Map[String, Seq[String]]()) { (acc, next) =>
          val visitiableNodes = next._2.filter(!_.equals(start))
          acc + (next._1 -> visitiableNodes)
        }
      else
        graph

    nodes.foldLeft(Nil: Seq[Seq[String]]) { (acc, dest) =>
      val path = current :+ dest
      if (dest == target) then
        acc :+ path
      else
        acc ++ find(dest, target, path, nextGraph)
    }

  def parse(currentGraph: Map[String, Seq[String]], input: String): Map[String, Seq[String]] =
    def insertEdge(start: String, stop: String, g: Map[String, Seq[String]]) =
      val current = g.getOrElse(start, Nil)
      val nextEdges = current :+ stop
      g + (start -> nextEdges)

    val nodes = input.split("-")
    val n1 = nodes(0)
    val n2 = nodes(1)

    insertEdge(n1, n2,
      insertEdge(n2, n1, currentGraph)
    )


  def pathsThrough(graph: Map[String, Seq[String]]): Seq[Seq[String]] =
    val start = "start"
    val end = "end"

    find(start, end, Seq(start), graph)
}

@main def aoc2021Day12_01(): Unit =
  import com.gitlab.bable5.aoc2021.AoC2021_12.{pathsThrough, parse}
  import com.gitlab.bable5.aoc2021.Utils.time
  val graph = ResourceFileReader("day12.input")
    .foldLeft(Map())(parse)

  println(
    time {
      pathsThrough(graph).size
    }
  )
