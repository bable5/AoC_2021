package com.gitlab.bable5.aoc2021

trait Direction {
  val value: Int
}

case class Position(x: Int = 0, y: Int = 0, aim: Int = 0)

case class Up(value: Int) extends Direction

case class Down(value: Int) extends Direction
case class Forward(value: Int) extends Direction

def parse(s: String): Direction =
  val parts = s.split(" ")
  val value = parts(1).toInt
  parts(0) match {
    case "forward" => Forward(value)
    case "up" => Up(value)
    case "down" => Down(value)
  }

@main def day0201(): Unit =
  val finalPosition = ResourceFileReader("day02.input")
    .map(parse)
    .foldLeft(Position()) { (p, next) =>
      next match {
        case Up(v) => p.copy(y = p.y - v)
        case Down(v) => p.copy(y = p.y + v)
        case Forward(v) => p.copy(x = p.x + v)
      }
    }
  println(finalPosition)
  println(finalPosition.x * finalPosition.y)

@main def day0202(): Unit =
  val finalPosition = ResourceFileReader("day02.input")
    .map(parse)
    .foldLeft(Position()) { (p, next) =>
      next match {
        case Up(v) => p.copy(aim = p.aim - v)
        case Down(v) => p.copy(aim = p.aim + v)
        case Forward(v) => p.copy(x = p.x + v, y = p.y + p.aim * v)
      }
    }
  println(finalPosition)
  println(finalPosition.x * finalPosition.y)
