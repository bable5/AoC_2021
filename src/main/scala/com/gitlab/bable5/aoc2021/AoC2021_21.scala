package com.gitlab.bable5.aoc2021

import scala.util.control.NonFatal

object AoC2021_21 {

}

case class Player(score: Int, position: Int, name: String) {
  def addToScore(delta: Int) = this.copy(score = score + delta)

  override def toString(): String = s"$name: Space $position for a total score of $score"
}

case class DeterministicDie(value: Int) {
  def roll(): DeterministicDie =
    if value <= 100 then DeterministicDie(1) else DeterministicDie(this.value + 1)
}

@main def benSolution() : Unit =
  println(new AoC2021_01_Ben().playDeterministic(4, 8, "Example"))
  println(new AoC2021_01_Ben().playDeterministic(7, 4, ""))

@main def modCheck(): Unit = println(s"${11 % 10}")

@main def aoc2021Day21_01(): Unit =
  var activePlayer = Player(0, 7, "P1")
  var otherPlayer = Player(0, 4, "P2")
  val maxPosition = 10;
  var die = DeterministicDie(0)
  var rolls = 0

  while (activePlayer.score < 1000 && otherPlayer.score < 1000)
  do
    die = die.roll()
    val r1 = die.value
    die = die.roll()
    val r2 = die.value
    die = die.roll()
    val r3 = die.value
    val moves = r1 + r2 + r3 //TODO: There's a simpler expression in here
    rolls = rolls + 3
    println(s"$r1, $r2, $r3")

    var nextPosition = activePlayer.position + moves
    if (nextPosition > maxPosition) then
      nextPosition = nextPosition % maxPosition
      // Positions are 1 - 10, 10 % 10 == 0. Force the counter back
      if(nextPosition == 0) then nextPosition = 10
      println(s"Next position: $nextPosition")

//    println(s"Turn $i Move ${moves} from ${activePlayer.position} to $nextPosition")

    val nextPlayer = activePlayer.addToScore(nextPosition).copy(position = nextPosition)

    println(s"$nextPlayer")

    activePlayer = otherPlayer
    otherPlayer = nextPlayer
    println("############################################")

  println("-------------------FINAL---------------------")
  println(s"Rolls: $rolls")
  println(s"$activePlayer")
  println(s"$otherPlayer")
  val minScore = activePlayer.score.min(otherPlayer.score)
  println(s"${minScore * rolls}")
