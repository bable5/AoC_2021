package com.gitlab.bable5.aoc2021

object AoC2021_07 {
  def part2AlignmentCost(start: Int, dest: Int): Int = 
      val d = Math.abs(dest - start)
      (d*(d+1))/2      

  def part1AlignmentCost(start: Int, dest: Int): Int = Math.abs(dest - start)

  def alignmentCost(pos: Int, positions: Seq[Int], costFunction: (Int, Int) => Int): Int = 
      positions.map(costFunction(pos, _))
        .sum

  def findMinimimCost(positions: Seq[Int], costFunction: (Int, Int) => Int): Int = 
      var min = Integer.MAX_VALUE
      for(i <- 1 to positions.size) 
        val cost = alignmentCost(i, positions, costFunction)
        if(cost < min) then min = cost
      
      min
}

@main def aoc2021Day07_01(): Unit = 
    import com.gitlab.bable5.aoc2021.AoC2021_07.*
    val input = ResourceFileReader("day07.input")
    .next
    .split(",")
    .map(_.toInt)
    println(findMinimimCost(input, part1AlignmentCost))

@main def aoc2021Day07_02(): Unit = 
    import com.gitlab.bable5.aoc2021.AoC2021_07.*
    val input = ResourceFileReader("day07.input")
    .next
    .split(",")
    .map(_.toInt)
    println(findMinimimCost(input, part2AlignmentCost))
    