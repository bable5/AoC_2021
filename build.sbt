val scala3Version = "3.1.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "advent_of_code_2021",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,
    libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3.0",
    libraryDependencies += "org.typelevel" %% "munit-cats-effect-3" % "1.0.6" % Test,
    libraryDependencies += "org.scalameta" %% "munit" % "1.0.0-M1" % Test
  )
